package events;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.bson.types.ObjectId;

public class Event {
    private ObjectId id;
    private String stringId;
    private String name;
    private String where;
    private Date when;
    private String description; // uploaded as file

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public Date getWhen() {
        return when;
    }

    public void setWhen(Date when) {
        this.when = when;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStringId() {
        return id.toString();
    }

    public void setStringId(String stringId) {
        this.id = new ObjectId(stringId);
    }
    
    
}
