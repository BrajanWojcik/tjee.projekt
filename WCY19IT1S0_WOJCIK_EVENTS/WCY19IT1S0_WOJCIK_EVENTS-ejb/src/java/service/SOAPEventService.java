package service;

import events.Event;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;

@WebService(serviceName = "SOAPEventService")
@Stateless()
public class SOAPEventService {

    private final DBManager db = new DBManager();
    
    /**
     * Web service operation
     * @return 
     */
    @WebMethod(operationName = "eventsList")
    public List<Event> eventsList() {
        return db.list();
    }

    /**
     * Web service operation
     * @param eventId
     * @return 
     */
    @WebMethod(operationName = "download")
    public Event download(@WebParam(name = "eventId") String eventId) {
        return db.get(eventId);
    }
    
    
    /**
     * Web service operation
     * @param eventId 
     */
    @WebMethod(operationName = "delete")
    public void delete(@WebParam(name = "eventId") String eventId) {
      db.delete(eventId);
    }   

    /**
     * Web service operation
     * @param event
     */
    @WebMethod(operationName = "upload")
    @Oneway
    public void upload(@WebParam(name = "event") Event event) {
        db.insert(event);
    }
    
    
}