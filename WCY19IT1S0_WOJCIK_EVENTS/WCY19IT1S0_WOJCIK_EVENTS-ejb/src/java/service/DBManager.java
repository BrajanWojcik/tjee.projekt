package service;

import com.mongodb.MongoClient;
import static com.mongodb.MongoClient.getDefaultCodecRegistry;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import events.Event;
import java.util.ArrayList;
import org.bson.codecs.configuration.CodecProvider;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class DBManager {
    
    private MongoCollection<Event> collection;

    public DBManager(){
        //prepare POJO Codec registry
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
        MongoCredential cred = MongoCredential.createCredential("student", "studenci_2020", "student-TJEE".toCharArray());
        MongoClient mongoClient = new MongoClient(new ServerAddress("172.16.65.2"), cred, MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
        
        
        MongoDatabase db = mongoClient.getDatabase("studenci_2020");
        
        collection = db.getCollection("WCY19IT1S0_WOJCIK_EVENTS", Event.class);
        if(collection == null){
            db.createCollection("WCY19IT1S0_WOJCIK_EVENTS");
            collection = db.getCollection("WCY19IT1S0_WOJCIK_EVENTS", Event.class);
        }
        
        collection = collection.withCodecRegistry(pojoCodecRegistry);

    }
    
    public List<Event> list(){
        return (List<Event>) collection.find(not(eq("name", null))).into(new ArrayList<>());
    }
    
    public Event get(String id){
        return (Event) collection.find(eq("stringId", id)).first();
    }
    
    public void insert(Event event){
        collection.insertOne(event);
    }
    
    public void delete(String id){
        collection.updateOne(eq("stringId", id), combine(set("name", null), set("description", null)));
    }
    
}
