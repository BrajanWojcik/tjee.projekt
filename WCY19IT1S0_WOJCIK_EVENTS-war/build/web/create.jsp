<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New event</title>
    </head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsee/css/components.css">
    <link rel="stylesheet" href="responsee/css/responsee.css">
    <link rel="stylesheet" href="responsee/css/icons.css">
    <script type="text/javascript" src="responsee/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="responsee/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="responsee/js/modernizr.js"></script>
    <body id="page-body">
        <h1>Insert new cultural event to list</h1>
        <jsp:useBean class="page.Controller" id="cont" scope="application"/>
        <% cont.setErrorMessage(null); %>
        <c:if test="${cont.errorMessage != null}">
            <br/><br/>
            <span style="color: red"><c:out value="${cont.errorMessage}"/></span>
        </c:if>
        <br/><br/>
        <div class="size-960">
            <form class="customform" action="created.jsp" method="POST" enctype="multipart/form-data">
                <div class="line">
                    <div class="s-12 l-12 padding" style="text-align: left;">
                        <label for="name">Event name</label>
                        <input name="name" id="name" placeholder="Enter event name..." type="text" required="true"/>
                    </div>
                    
                    <div class="s-12 l-6 padding " style="text-align: left;">
                        <label for="date">Date</label>
                        <input name="date" id="date" type="date" required="true" max="2099-12-"/>
                    </div>
                    
                    <div class="s-12 l-6 padding" style="text-align: left;">
                        <label for="time">Time</label>
                        <input name="time" id="time" type="time" required="true"/>
                    </div>
                    
                    <div class="s-12 l-6 padding" style="text-align: left;">
                        <label for="address">Address</label>
                        <input name="address" id="address" placeholder="Address" type="text" required="true"/>
                    </div>
                    
                    <div class="s-12 l-6 padding" style="text-align: left;">
                        <label for="description">Description (text file)</label>
                        <input name="description" id="description" type="file" accept=".txt" required="true"/>
                    </div>

                    <div class="s-6 l-3 right" style="text-align: left;">
                        <button type="submit" class="button rounded-full-btn submit-btn margin-bottom">Save event</button>
                    </div>
                </div>
            </form>
        </div>
        <script type="text/javascript" src="responsee/js/responsee.js"></script>
    </body>
</html>
