<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Event deletion</title>
    </head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsee/css/components.css">
    <link rel="stylesheet" href="responsee/css/responsee.css">
    <link rel="stylesheet" href="responsee/css/icons.css">
    <script type="text/javascript" src="responsee/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="responsee/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="responsee/js/modernizr.js"></script>
    <body id="page-body">
        
        <jsp:useBean class="page.Controller" id="cont" scope="application"/>
        <% 
            cont.processDelete(request.getParameter("eventId"));
        %>
        <br/><br/>
        <c:if test="${cont.errorMessage != null}">
            <h1 style="color: red"><c:out value="${cont.errorMessage}"/></h1>
        </c:if>
        <c:if test="${cont.errorMessage == null}">
            <h1>Event deleted!</h1>
        </c:if>
        
        <a style="margin-top: 10px;" class="button rounded-full-btn submit-btn margin-bottom" href="calendar.jsp"><i class="icon-home"></i> Back to home page</a>
        <script type="text/javascript" src="responsee/js/responsee.js"></script>
    </body>
</html>
