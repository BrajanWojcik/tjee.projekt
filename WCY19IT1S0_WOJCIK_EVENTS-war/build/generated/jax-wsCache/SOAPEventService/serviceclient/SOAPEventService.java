
package serviceclient;

import java.util.List;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SOAPEventService", targetNamespace = "http://service/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SOAPEventService {


    /**
     * 
     * @param event
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "upload", targetNamespace = "http://service/", className = "serviceclient.Upload")
    @Action(input = "http://service/SOAPEventService/upload")
    public void upload(
        @WebParam(name = "event", targetNamespace = "")
        Event event);

    /**
     * 
     * @return
     *     returns java.util.List<serviceclient.Event>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "eventsList", targetNamespace = "http://service/", className = "serviceclient.EventsList")
    @ResponseWrapper(localName = "eventsListResponse", targetNamespace = "http://service/", className = "serviceclient.EventsListResponse")
    @Action(input = "http://service/SOAPEventService/eventsListRequest", output = "http://service/SOAPEventService/eventsListResponse")
    public List<Event> eventsList();

    /**
     * 
     * @param eventId
     * @return
     *     returns serviceclient.Event
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "download", targetNamespace = "http://service/", className = "serviceclient.Download")
    @ResponseWrapper(localName = "downloadResponse", targetNamespace = "http://service/", className = "serviceclient.DownloadResponse")
    @Action(input = "http://service/SOAPEventService/downloadRequest", output = "http://service/SOAPEventService/downloadResponse")
    public Event download(
        @WebParam(name = "eventId", targetNamespace = "")
        String eventId);

    /**
     * 
     * @param eventId
     */
    @WebMethod
    @RequestWrapper(localName = "delete", targetNamespace = "http://service/", className = "serviceclient.Delete")
    @ResponseWrapper(localName = "deleteResponse", targetNamespace = "http://service/", className = "serviceclient.DeleteResponse")
    @Action(input = "http://service/SOAPEventService/deleteRequest", output = "http://service/SOAPEventService/deleteResponse")
    public void delete(
        @WebParam(name = "eventId", targetNamespace = "")
        String eventId);

}
