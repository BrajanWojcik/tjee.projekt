<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Events Calendar</title>
    </head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsee/css/components.css">
    <link rel="stylesheet" href="responsee/css/responsee.css">
    <link rel="stylesheet" href="responsee/css/icons.css">
    <script type="text/javascript" src="responsee/js/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="responsee/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="responsee/js/modernizr.js"></script>
    <body id="page-body">
        <h1>Cultural Events Exchange Portal</h1>
        <jsp:useBean class="page.Controller" id="cont" scope="application"/>
        <c:set var="eventList" value="${cont.eventsList()}"/>
        <c:if test="${cont.errorMessage != null}">
            <br/><br/>
            <span style="color: red"><c:out value="${cont.errorMessage}"/></span>
        </c:if>
        <br/><br/>
        <div class="size-960">
            <div class="line">
                <a class="button rounded-full-btn submit-btn margin-bottom" href="create.jsp"><i class="icon-plus"></i>Create event</a>
                <div class="accordion">
                    <c:forEach var="ev" items="${eventList}">
                        <div class="accordion-section">
                            <h2 class="accordion-title">${ev.name}</h2>
                            <div class="accordion-content">
                                <p><span class="iconspan"><i class="icon-calendar"></i></span>${cont.formatDate(ev.when)}</p>
                                <p><span class="iconspan"><i class="icon-clock"></i></span>${cont.formatTime(ev.when)}</p>
                                <p><span class="iconspan"><i class="icon-placepin"></i></span>${ev.where}</p>
                                <p><span class="iconspan"><i class="icon-pen"></i></span>${ev.description}</p>
                                <a style="margin-top: 10px;" class="button rounded-full-btn margin-top" href="download.jsp?eventId=${ev.stringId}"><i class="icon-download"></i>Download</a>
                                <a style="margin-top: 10px;" class="modal-button button rounded-full-btn cancel-btn margin-top" data-modal="modal-${ev.stringId}"><i class="icon-cross_mark"></i>Delete</a>
                                
                                <!-- Modal content -->
                                <div class="modal" style="width: 400px;" id="modal-${ev.stringId}">
                                    <h3>Are you sure?</h3>
                                    <p>
                                        Do you really want to delete event <strong>${ev.name}</strong>?
                                    </p>
                                    
                                    <a style="margin-top: 10px;" class="button rounded-full-btn cancel-btn" href="delete.jsp?eventId=${ev.stringId}">Delete</a>
                                    <a style="margin-top: 10px;" class="modal-close-button button rounded-full-btn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="responsee/js/responsee.js"></script>
    </body>
</html>
