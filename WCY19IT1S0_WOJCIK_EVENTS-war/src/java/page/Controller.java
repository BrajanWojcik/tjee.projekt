package page;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceRef;
import serviceclient.Event;
import serviceclient.SOAPEventService_Service;

@Singleton
@LocalBean
public class Controller {

    @WebServiceRef(wsdlLocation = "10.103.1.26:8080/SOAPEventService/SOAPEventService?wsdl")
    private SOAPEventService_Service service = new SOAPEventService_Service();

    private String errorMessage;

    public List<serviceclient.Event> eventsList(){
        errorMessage = null;
        try{
            List<serviceclient.Event> list = service.getSOAPEventServicePort().eventsList();
            return list;
        } catch(Exception e){
            errorMessage = "Error loading events list: " + e.getMessage();
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        }
    }
    
    public String formatDate(XMLGregorianCalendar cal){
        return new SimpleDateFormat("dd.MM.yyyy").format(cal.toGregorianCalendar().getTime());
    }
    
    
    public String formatTime(XMLGregorianCalendar cal){
        return new SimpleDateFormat("HH:mm").format(cal.toGregorianCalendar().getTime());
    }
    
    public void processDownload(HttpServletRequest request, HttpServletResponse response){
        errorMessage = null;
        try{
            String id = request.getParameter("eventId");
            Event e = service.getSOAPEventServicePort().download(id);
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + e.getName() + ".txt\"");
            response.getOutputStream().write(e.getDescription().getBytes());
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (Exception e){
            errorMessage = "Error downloading event data!";
        }
    }
    
    
    public void processDelete(String id){
        errorMessage = null;
        try{
            service.getSOAPEventServicePort().delete(id);
        } catch (Exception e){
            errorMessage = "Error deleting event data!";
        }
    }
    
    public void processUpload(HttpServletRequest request){
        errorMessage = null;
        try{
            Event e = new Event();
            
            //set event name
            e.setName(request.getParameter("name"));
            
            //set event date and time 
            String datetime = request.getParameter("date") + "T" + request.getParameter("time");
            Date eventDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(datetime);
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(eventDate);
            XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            e.setWhen(xmlCal);
            
            //set event address
            e.setWhere(request.getParameter("address"));
            
            //set event description
            Part descPart = request.getPart("description");
            byte[] description = new byte[(int)descPart.getSize()];
            descPart.getInputStream().read(description);
            e.setDescription(new String(description));
            service.getSOAPEventServicePort().upload(e);
        } catch (Exception e){
            e.printStackTrace();
            errorMessage = "Error uploading event!";
        }
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
